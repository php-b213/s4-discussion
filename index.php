<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S4: Access Modifiers and Encapsulation</title>
</head>
<body>
	<h1>Access Modifiers</h1>
	<h2>Building Variable</h2>
	<p><?php //echo $building->name;?></p>

	<h2>Condominium Variables</h2>
	<p><?php //echo $condominium->name; ?></p>

	<h1>Encapsulation</h1>
	<p><!-- Invoke the getter -->
		The name of the condominium is <?php echo $condominium->getName(); ?>
	</p>
		<?php $condominium->setName('Enzo Tower'); ?>
		<p>The name of the condominium has been changed to <?php echo $condominium->getName();?></p>

	<p><!-- Invoke the getter -->
		The condo's number of floors: <?php echo $condominium->getFloors(); ?>
	</p>
		<?php $condominium->setFloors(10); ?>
		<p>The condo's number of floors has been changed to:<?php echo $condominium->getFloors();?></p>

	<!-- the get/setter for this in found in line 41 of code.php-->
		<!-- <p>The number of floors in the condo has been changed to <?php //echo $condo->setFloors(10)->getFloors(); ?>.</p> -->
</body>
</html>