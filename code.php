<?php
	class Building {
	//TYPES OF MODIFIERS:
		//1. 'private' access modifier => disables direct access to an object's property or methods.

		//2. 'protected' access modifier => allows inheritance of properties and methods of child classes. However, it will still disable direct access to its properties and methods.

		protected $name;
		protected $floors;
		protected $address;


		public function __construct($name, $floors, $address){
			$this->name = $name;
			$this->floors = $floors;
			$this->address = $address;
		}
	}


	class Condominium extends Building {
		//getName() => reserve keyword of php
		//getters and setters
		public function getName(){
			return $this->name;
		}

		public function setName($name){
			$this->name = $name;
		}

		public function getFloors(){
			return $this->floors;
		}

		public function setFloors($floors){
			$this->floors = $floors;
		}

		/*Another way of shortening the code:
			public function setFloors($floors){
			        $this->floors = $floors;
			        return $this;
			}
		
		*/

	}
		//Instantiation 
		$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon city, Philippines');
		
		//Instantiation 
		$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');
?>